<?php

namespace Tests\Feature;

use Tests\TestCase;

class ChatMessageTest extends TestCase
{
    /**
     * Test Api home page response
     *
     * @return void
     */
    public function testApiHomeTest()
    {
        $response = $this->get('/');
        $response->assertStatus(200);
        $hello = $response->json();
        $this->assertEquals('world', $hello['hello']);
    }

    /**
     * Test Api home page response
     *
     * @return void
     */
    public function testApiHealthTest()
    {
        $response = $this->get('/api/health');
        $response->assertStatus(200);
        $healfy = $response->json();

        $this->assertEquals('healthy', $healfy['mysql']);
        $this->assertEquals('healthy', $healfy['mysql_migrations']);
        $this->assertIsFloat( floatval($healfy['response_time_ms']));
        $this->assertIsInt(intval($healfy['chat_messages_count']));
    }

    /**
     * Test Api home page response
     *
     * @return void
     */
    public function testApiMessageTest()
    {
        $response = $this->get('/api/message');
        $response->assertStatus(200);
        $messages = $response->json();

        $this->assertIsArray($messages);
    }
}
