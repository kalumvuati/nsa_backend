<?php

namespace Tests\Unit;

use App\Models\ChatMessages;
use Tests\TestCase;

class ChatMessageTest extends TestCase
{
    /**
     *
     * @return void
     */
    public function testChatMessageTest()
    {
        $chatMessage = new ChatMessages();
        $chatMessage->setAttribute('message', 'Mon message');
        $chatMessage->setAttribute('username', 'Nom utilisateur très long');
        $this->assertEquals('Mon message', $chatMessage->getAttribute('message'));
        $this->assertEquals('Nom utilisateur très long', $chatMessage->getAttribute('username'));
    }
}
