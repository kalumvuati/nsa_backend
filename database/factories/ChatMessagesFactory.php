<?php

/** @var Factory $factory */

use App\Models\ChatMessages;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(ChatMessages::class, function (Faker $faker) {
    return [
        'username' => $faker->name,
        'message' => $faker->sentence,
    ];
});
