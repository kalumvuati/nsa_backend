<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Redirect;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $comments =Comment::orderBy('id')->get();
        $params = ['comments' => $comments];
        return view('comment/index')->with($params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $status = 'ok';

        //TODO - to user try
        try {
            $request->validate([
                'message' => ['required'],
                'username' => ['required'],
            ]);

            $comment = [
                'username'=> $request->username,
                'message'=> $request->message,
            ];

            $country = Comment::create($comment);
            $country->save();
        } catch (\Exception $exception) {
            $status = 'failure';
            $comment = [
                'username'=> $request->username,
                'message'=> $request->message,
            ];
        }

        //Send message
        return response()->json(['status' => $status, 'comment' => $comment]);
    }

    /**
     * Display the specified resource.
     *
     * @param Comment $comment
     * @return Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Comment $comment
     * @return Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Comment $comment
     * @return Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Comment $comment
     * @return JsonResponse
     */
    public function destroy(Comment $comment)
    {
        $status = 'success';
        $comment = 'Comment was deleted successfully';
        try {
            $comment->delete();
        } catch (\Exception $exception) {
            $status = 'failure';
            $comment = 'Comment was not deleted';
        }

        return response()->json(['status' => $status, 'comment' => $comment]);
    }
}
